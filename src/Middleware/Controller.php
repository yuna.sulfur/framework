<?php

namespace Sulfur\Middleware;

use Sulfur\Middleware as MiddlewareInterface;

use Sulfur\Container;
use Sulfur\Request;
use Sulfur\Response;

class Controller implements MiddlewareInterface
{

	/**
	 * Container instance
	 * @var Sulfur\Container
	 */
	protected $container;


	/**
	 * Create a controller
	 * @param Sulfur\Container $container
	 */
	public function __construct(Container $container)
	{
		$this->container = $container;
	}


	/**
	 * Run the middleware
	 * @param Sulfur\Request $request
	 * @param Sulfur\Response $response
	 * @param callable $next
	 * @return Sulfur\Response
	 */
	public function __invoke(
        Request $request,
        Response $response,
        callable $next
    ) {
		// get handler
		$handler = $request->get('handler');

		// create & execute a controller-action
		if($handler && strpos($handler, '@') !== false){

			$parts = explode('@', $handler);
			$controller =  $this->container->get($parts[0]);

			$attributes = $request->attributes();
			$args = [
				':request' => $request,
				':response' => $response
			];
			foreach($attributes as $name => $value){
				$args[':' . $name] = $value;
			}
			// get result from controller action
			$result = $this->container->call([$controller, $parts[1]], $args, $handler);

			// returned false, stop execution of middleware
			if($result === false) {
				return $response;
			}

			// returned is not null: set the response body
			// if nothing was returned, dont set the body
			if($result !== null) {
				$response->body($result);
			}
		}
		return $next($request, $response);

	}
}