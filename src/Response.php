<?php

namespace Sulfur;

interface Response
{
	/**
	 * Helper to redirect to a url
	 * @param string $url
	 * @param int $status
	 */
	public function redirect($url, $status = 301);


	/**
	 * Set status
	 * @param int $code
	 * @param string $message
	 * @return Sulfur\Response
	 */
	public function status($code, $message = '');


	/**
	 * Set a Cookie
	 * @param string $name
	 * @param string $value
	 * @param int $expire
	 * @param string $path
	 * @param string $domain
	 * @param boolean $secure
	 * @param boolean $http
	 */
	public function cookie($name, $value, $expire = 0, $path = '/', $domain = null, $secure = null, $http = true);


	/**
	 * Add a header
	 * @param string $name
	 * @param string $value
	 * @return Sulfur\Response
	 */
	public function header($name, $value);


	/**
	 * Append to body
	 * @param string|null $value The body value or null to get the current body
	 * @param bool $append Append to the current body or replace current body
	 * @return Sulfur\Response
	 */
	public function body($value = null, $append = false);


	/**
	 * Send the status, headers and body to the browser
	 */
	public function send();
}