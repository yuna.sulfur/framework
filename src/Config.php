<?php

namespace Sulfur;

class Config
{
	/**
	 * Paths to load resources from
	 * @var array
	 */
	protected $paths = [];


	/**
	 * Replacements vars
	 * @var array
	 */
	protected $replace = [];


	/**
	 * Token for placeholders
	 * @var array
	 */
	protected $token = ['{{', '}}'];


	/**
	 * Pattern for placeholders
	 * @var array
	 */
	protected $pattern = '';


	/**
	 * Hot-cached resources
	 * @var array
	 */
	protected $resources = [];


	/**
	 * Whether extra resources were loaded
	 * @var boolean
	 */
	protected $changed = false;


	/**
	 * Constructor
	 * @param string|array $paths
	 * @param array $cache
	 */
	public function __construct($paths, $replace = [], $token = null)
	{
		$this->paths = $paths;
		$this->replace = $replace;
		if(is_array($token) && count($token) >= 2) {
			$this->token[0] = $token[0];
			$this->token[1] = $token[1];
		}
		$this->pattern = '/' . preg_quote($this->token[0]) . '\s*([a-zA-Z0-9\-\_\.]+)\s*' . preg_quote($this->token[1]) . '/';
	}


	/**
	 * Get or set cached data
	 * @param array $resources
	 * @return mixed
	 */
	public function resources($resources = null)
	{
		if($resources === null) {
			return $this->resources;
		} else {
			$this->resources = array_merge($this->resources, $resources);
		}
	}


	/**
	 * Check whether extra resources were loaded (and should be cached)
	 * @return type
	 */
	public function changed()
	{
		return $this->changed;
	}


	/**
	 * Get a resource or a sub-key of a resource
	 * @param string $resource
	 * @param string|array $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function __invoke($resource, $key = null, $default = null)
	{
		return $this->resource($resource, $key, $default);
	}


	/**
	 * Get a resource or a sub-key of a resource
	 * @param string $resource
	 * @param string|array $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function resource($resource, $key = null, $default = null)
	{
		$data = $this->data($resource);
		if($key === null){
			return $data;
		} else {
			return $this->find($data, $key, $default);
		}
	}


	/**
	 * Get a resource by calling a method with it's name
	 * @param string $method
	 * @param array $arguments
	 * @return mixed
	 */
	public function __call($method, $arguments)
	{
		return $this->__invoke(
			str_replace(['__', '_'], [':', DIRECTORY_SEPARATOR], $method),
			isset($arguments[0]) ? $arguments[0]: null,
			isset($arguments[1]) ? $arguments[1]: null
		);
	}


	/**
	 * Get resource data
	 * @param string $resource
	 * @return array
	 */
	protected function data($resource)
	{
		if (isset($this->resources[$resource])) {
			return $this->resources[$resource];
		}

		if(strpos($resource, ':') !== false) {
			// find a resource at a named path
			$parts = explode(':', $resource);
			if(isset($this->paths[$parts[0]])) {
				$full = $this->paths[$parts[0]] . $parts[1] . '.php';
				if (file_exists($full)) {
					$data = include($full);
				}
			}
		} else {
			// find the first available resource
			foreach ($this->paths as $path) {
				$full = $path . $resource . '.php';
				if (file_exists($full)) {
					$data = include($full);
					break;
				}
			}
		}

		// normalize data
		$data = isset($data) && is_array($data) ? $data : [];

		// replace placeholders
		array_walk_recursive($data, function(&$item, $key) {
			if(is_string($item) && strpos($item, $this->token[0]) !== false) {
				preg_match_all($this->pattern, $item, $matches);
				$find = [];
				$replace = [];
				foreach($matches[1] as $index => $match) {
					if(isset($this->replace[$match])) {
						$find[] = $matches[0][$index];
						$replace[] = $this->replace[$match];
					}
				}
				$item = str_replace($find, $replace, $item);
			}
		});

		$this->changed = true;
		$this->resources[$resource] = $data;
		return $data;
	}


	/**
	 * Get a variable from an array with dot notation or path-array
	 * @param array $data
	 * @param string|array $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function find($data, $key, $default = null)
	{
		// return all
		if ( ! $key) {
			return $data;
		}

		if (is_array($key)) {
			// key array provided
			// easy, keys with a dot are provided as-is in the array
			$walker = $data;
			while ($part = array_shift($key)) {
				if (is_array($walker) && is_string($part) && isset($walker[$part])) {
					// go deeper
					$walker = $walker[$part];
				} else {
					// not here, done
					return $default;
				}
			}
			// return what we ended up with
			return $walker;
		} else {
			// dotted key provided
			// hard, because data keys can also contain dots
			$walker = $data;
			$search = '';
			$separator = '';
			$parts = explode('.', $key);
			$remaining = count($parts);
			while ($part = array_shift($parts)) {
				// remaing parts decreased
				$remaining--;
				// build a search key to look for in the current walker
				$search .= $separator . $part;
				$separator = '.';

				if (is_array($walker) && isset($walker[$search])) {
					// search key is found
					// get the value of the walker and set it as the new walker
					$walker = $walker[$search];

					// reset the search key
					$separator = '';
					$search = '';

					if($remaining === 0) {
						// there are no more parts left: we found it
						return $walker;
					}
				} elseif ($remaining === 0) {
					// not found and no parts left: we failed
					return $default;
				}
			}
		}
	}
}