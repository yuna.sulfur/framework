<?php

namespace Sulfur\Console;

use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class Command extends BaseCommand
{

	/**
	 * Input object
	 * @var InputInterface
	 */
	protected $input;

	/**
	 * Ouptu object
	 * @var OutputInterface
	 */
	protected $output;

	/**
	 * Array with arguments. See https://symfony.com/doc/current/console/input.html for format. use arrays for multi-argument arguments
	 * @var array
	 */
	protected $arguments = [];

	/**
	 * Array with options. See https://symfony.com/doc/current/console/input.html for format. use arrays for multi-argument options
	 * @var array
	 */
	protected $options = [];

	/**
	 * The description of the command
	 * @var string
	 */
	protected $description = '';


	/**
	 * The helptext of the command
	 * @var string
	 */
	protected $help = '';


	/**
	 * Empty constructor instead of original constructor
	 * Use it to pass in dependencies
	 */
	public function __construct() {}


	/**
	 * Do the actual handling of the command
	 * @return mixed
	 */
	public function handle() {}


	/**
	 * Startup of the command
	 * Set definition, description and help
	 * Set options from array
	 * Set arguments from array
	 * Call configure for extra configuration
	 */
	public function init()
	{
		$this->setDefinition(new InputDefinition());
		$this->setDescription($this->description);
		$this->setHelp($this->help);

		foreach($this->options as $option) {
			if(! is_array($option)) {
				$option = [$option];
			}
			$this->addOption(...$option);
		}

		foreach($this->arguments as $argument) {
			if(! is_array($argument)) {
				$argument = [$argument];
			}
			$this->addArgument(...$argument);
		}

		$this->configure();
	}


	/**
	 * Execution of the command. Don't overwrite, use 'handle' to be able to use helper functions
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function execute(InputInterface $input, OutputInterface $output)
    {
		$this->input = $input;
		$this->output = $output;

		// call the handle method instead
		$this->handle();
    }


	/**
	 * Output of the command
	 * @param array $messages
	 */
	protected function write($messages)
	{
		if(! is_array($messages)) {
			$messages = [$messages];
		}
		$this->output->writeln($messages);
	}


	/**
	 * Get an argument or all arguments
	 * @param string $argument
	 * @return string|array
	 */
	protected function argument($argument = null)
	{
		if($argument === null) {
			return $this->input->getArguments();
		} else {
			return $this->input->getArgument($argument);
		}
	}


	/**
	 * Get an option or all options
	 * @param string $option
	 * @return string|array
	 */
	protected function option($option = null)
	{
		if($option === null) {
			return $this->input->getOptions();
		} else {
			return $this->input->getOption($option);
		}
	}
}