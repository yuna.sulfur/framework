<?php

namespace Sulfur\Http;

use Symfony\Component\HttpFoundation\Request as BaseRequest;
use Sulfur\Request as RequestInterface;

class Request implements RequestInterface
{

	/**
	 * The raw request object
	 * @var Symfony\Component\HttpFoundation\Request
	 */
	protected $request;

	/**
	 * Bag of extra attributes
	 * @var array
	 */
	protected $attributes = [];


	/**
	 * Post data we are using
	 * @var array
	 */
	protected $post = [];


	/**
	 * Create a new request
	 * @param array $server
	 * @param array $get
	 * @param array $post
	 * @param array $cookie
	 * @param array $files
	 */
	public function __construct(
		$server = null,
		$get = null,
		$post = null,
		$cookie = null,
		$files = null
	)
	{
		// create postdata
		$this->post = $post ?: $_POST;

		// create request
		$this->request= new BaseRequest(
			$get ?: $_GET,
			$this->post,
			[],
			$cookie ?: $_COOKIE,
			$files ?: $_FILES,
			$server ?: $_SERVER
		);
	}


    /**
     * {@inheritdoc}
     */
	public function scheme()
	{
		return $this->request->getScheme();
	}


    /**
     * {@inheritdoc}
     */
	public function domain()
	{
		return explode(':', $this->request->getHttpHost())[0];
	}


    /**
     * {@inheritdoc}
     */
	public function method()
	{
		return $this->request->getMethod();
	}


    /**
     * {@inheritdoc}
     */
	public function port()
	{
		$port = $this->request->getPort();
		if(! $port) {
			$port = 80;
		}
		return $port;
	}


    /**
     * {@inheritdoc}
     */
	public function base()
	{
		return $this->request->getBasePath();
	}


    /**
     * {@inheritdoc}
     */
	public function path($full = true)
	{
		if($full) {
			return $this->base() . $this->request->getPathInfo();
		} else {
			return $this->request->getPathInfo();
		}
	}


    /**
     * {@inheritdoc}
     */
	public function query($name = null, $default = null)
	{
		if($name !== null) {
			return $this->request->query->get($name, $default);
		} else {
			return $this->request->query->all();
		}
	}


    /**
     * {@inheritdoc}
     */
	public function post($name = null, $default = null)
	{

		if($name !== null) {
			return $this->request->request->get($name, $default);
		} else {
			return $this->request->request->all();
		}
	}


    /**
     * {@inheritdoc}
     */
	public function qs()
	{
		return $this->request->getQueryString();
	}


    /**
     * {@inheritdoc}
     */
	public function ajax()
	{
		return $this->request->isXmlHttpRequest();
	}


    /**
     * {@inheritdoc}
     */
	public function file($name)
	{
		if($file = $this->request->files->get($name, false)) {
			return (object) [
				'path' => $file['tmp_name'],
				'name' => $file['name']
			];
		} else {
			return null;
		}
	}


    /**
     * {@inheritdoc}
     */
	public function header($name, $default = null)
	{
		return $this->request->headers->get($name, $default);
	}


	/**
     * {@inheritdoc}
     */
	public function server($name, $default = null)
	{
		return $this->request->server->get($name, $default);
	}


	/**
     * {@inheritdoc}
     */
	public function cookie($name, $default = null)
	{
		return $this->request->cookies->get($name, $default);
	}


	/**
     * {@inheritdoc}
	 */
	public function ip()
	{
		return $this->request->getClientIp();
	}


    /**
     * {@inheritdoc}
     */
	public function attributes(array $attributes = null)
	{
		if($attributes === null) {
			return $this->attributes;
		} elseif(is_array($attributes)) {
			$this->attributes = array_merge($this->attributes, $attributes);
			return $this;
		}
	}


    /**
     * {@inheritdoc}
     */
	public function get($attribute, $default = null)
	{
		if(isset($this->attributes[$attribute])) {
			return $this->attributes[$attribute];
		} else {
			return $default;
		}
	}


    /**
     * {@inheritdoc}
     */
	public function set($attribute, $value)
	{
		$this->attributes[$attribute] = $value;
		return $this;
	}
}