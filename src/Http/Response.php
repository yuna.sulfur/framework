<?php

namespace Sulfur\Http;

use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Sulfur\Response as ResponseInterface;

class Response implements ResponseInterface
{
	/**
	 * PSR7 response
	 * @var Symfony\Component\HttpFoundation\Response
	 */
	protected $response;

	/**
	 * Create a new response
	 */
	public function __construct()
	{
		$this->response = new BaseResponse();
	}


    /**
     * {@inheritdoc}
     */
	public function redirect($url, $status = 301)
	{
		$this->status($status);
		$this->header('location', $url);
		$this->send();
		exit;
	}


    /**
     * {@inheritdoc}
     */
	public function status($code, $message = '')
	{
		$this->response->setStatusCode($code, $message);
		return $this;
	}


	/**
     * {@inheritdoc}
     */
	public function cookie($name, $value, $expire = 0, $path = '/', $domain = null, $secure = null, $http = true)
    {
		$this->response->headers->setCookie(Cookie::create($name, $value, $expire, $path, $domain, $secure, $http));
	}


    /**
     * {@inheritdoc}
     */
	public function header($name, $value)
	{
		$this->response->headers->set($name, $value);
		return $this;
	}


    /**
     * {@inheritdoc}
     */
	public function body($value = null, $append = false)
	{
		if($value === null) {
			return $this->response->getContent();
		} else {
			if($append) {
				$this->response->setContent($this->response->getContent() . $value);
			} else {
				$this->response->setContent($value);
			}
			return $this;
		}
	}


    /**
     * {@inheritdoc}
     */
	public function send()
	{
		$this->response->send();
	}
}