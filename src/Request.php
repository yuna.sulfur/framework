<?php

namespace Sulfur;

interface Request
{

	/**
	 * Get the http / https
	 * @return string
	 */
	public function scheme();


	/**
	 * Get the domain
	 * @return string
	 */
	public function domain();


	/**
	 * Get request method
	 * @return string
	 */
	public function method();


	/**
	 * Get the port
	 * @return int
	 */
	public function port();


	/**
	 * Get the absolute path up to the executing front controller (index.php)
	 * Used to prefix routes with this base path, so site can run somewhere else than the root
	 * @return string
	 */
	public function base();



	/**
	 * Get the request path
	 * @param boolean $full Return path including base
	 * @return string
	 */
	public function path($full = true);


	/**
	 * Get a query string var
	 * @param string $name
	 * @param string $default
	 * @return string
	 */
	public function query($name = null, $default = null);


	/**
	 * Get a post var
	 * @param string $name
	 * @param string|array $default
	 * @return string|array
	 */
	public function post($name = null, $default = null);


	/**
	 * Get the whole query string
	 * @return string
	 */
	public function qs();


	/**
	 * Whether this the request is via ajax
	 * @return boolean
	 */
	public function ajax();


	/**
	 * Get info about a posted file
	 * @param string $name
	 * @return strClass|null
	 */
	public function file($name);


	/**
	 * Get the value of a header
	 * @param string $name
	 * @param string $default
	 */
	public function header($name, $default = null);


	/**
	 * Get the value of a server variable
	 * @param string $name
	 * @param string $default
	 */
	public function server($name, $default = null);


	/**
	 * Get the value of a cookie
	 * @param string $name
	 * @param string $default
	 */
	public function cookie($name, $default = null);

	
	/**
	 * Get the client-ip of a request
	 */
	public function ip();


	/**
	 * Set or get attributes
	 * @param array|null $attributes
	 * @return Sulfur\Request|array
	 */
	public function attributes(array $attributes = null);


	/**
	 * Get an attribute
	 * @param string $attribute
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($attribute, $default = null);


	/**
	 * Set an attribute
	 * @param string $attribute
	 * @param mixed $value
	 * @return \Sulfur\\Request
	 */
	public function set($attribute, $value);
}