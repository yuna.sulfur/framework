<?php

namespace Sulfur;

use Sulfur\Request;
use Sulfur\Response;

interface Middleware
{
	public function __invoke(Request $request, Response $response, callable $next);
}