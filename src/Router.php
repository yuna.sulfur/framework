<?php

namespace Sulfur;

interface Router
{
	/**
	 * Create a map or receive a pre-charted map
	 * @param array $map
	 * @return $this|array
	 */
	public function map($map = null);


	/**
	 * Try to match routes
	 * return matched route or false
	 *
	 * @param string $path The path to match
	 * @param string $method The method to match
	 * @param string $domain The domain to match
	 * @return array|boolean
	 */
	public function match($path, $method = 'GET' , $domain = null);


	/**
	 * Create a path from a route and params
	 * @param string $route
	 * @param array $params
	 * @return string
	 */
	public function path($route, $params = []);
}